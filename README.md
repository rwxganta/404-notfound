<!-- Please update value in the {}  -->

<h1 align="center">404 not found page</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://notfound-webpage.pages.dev/">
      Demo
    </a>
    <span> | </span>
    <a href="https://devchallenges.io/challenges/wBunSb7FPrIepJZAg0sY">
      Challenge
    </a>
  </h3>
</div>

<!-- TABLE OF CONTENTS -->

## Table of Contents

- [Overview](#overview)
  - [Built With](#built-with)
- [Contact](#contact)


<!-- OVERVIEW -->

## Overview

![screenshot](notfound.png)
![screenshot](notfound-mobile.png)


Heyy o/

So, this project help me a lot to come back to to the basic of HTML and CSS. I was starting to forget how to make media queries and display things with flexbox power!

[You can see this demo right here](https://notfound-webpage.pages.dev/)

### Built With

- HTML
- CSS

## Contact

- Website (Under construction)
- Twitter [@rwxganta](https://twitter.com/rwxganta)
- Mail me: lukamendoncadev@proton.me
